#include <stdio.h>

#include "matrix.h"

int main() {
    board brd;  // 班によってはここにたくさん定義する

    initBoard(&brd);  // 班によってはここにたくさん渡す
    display(&brd);    // 班によってはここにもたくさん渡す
    return 0;
}

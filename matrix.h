#ifndef __MATRIX_H
#define __MATRIX_H
#define N 100
#define MAX_PIECES 8
#define MAX_PLAYERS 2
#define MAX_X 3
#define MAX_Y 4
#define EMP -1

typedef enum { SENTE, GOTE } playerType;

typedef enum {
    LION,
    GIRAFFE,
    ELEPHANT,
    CHIKEN,
    HEN  //
} pieceTypeE;

typedef struct {
    int direction;  // {-1 or 1}
    char name[255];
} player;

typedef struct {
    int x;
    int y;
    pieceTypeE type;
    playerType player;
    char pieceString[10];
} piece;

typedef struct {
    piece pieces[MAX_PIECES];
    player players[MAX_PLAYERS];
    playerType turn;
    int squares[MAX_Y][MAX_X];
} board;

static char *pieceNames[] = {"L", "E", "G", "C", "H"};
static char *directionNames[MAX_PLAYERS + 1] = {"▽", "", "△"};

void changePosition(board *bp, piece *pip, int x, int y);
void promoteToHen(board *bp, piece *pip);
void demoteToChick(board *bp, piece *pip);
void changePlayer(board *bp, piece *pip, player *plp);
void updatePieceString(board *bp, piece *pip);
void display(board *bp);
void initBoard(board *bp);

char *directionString(player *plp);
#endif
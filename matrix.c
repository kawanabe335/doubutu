#include "matrix.h"

#include <stdio.h>

piece initPieces[MAX_PIECES] = {
    {1, 0, LION, GOTE, ""},     {0, 0, GIRAFFE, GOTE, ""},
    {2, 0, ELEPHANT, GOTE, ""}, {1, 1, CHIKEN, GOTE, ""},
    {1, 3, LION, SENTE, ""},    {0, 3, ELEPHANT, SENTE, ""},
    {2, 3, GIRAFFE, SENTE, ""}, {1, 2, CHIKEN, SENTE, ""},
};

// 初期状態の駒データ

void updatePieceString(board *bp, piece *pip) {
    sprintf(pip->pieceString, "%s(%s)", pieceNames[pip->type],
            directionNames[bp->players[pip->player].direction + 1]);
}

void initBoard(board *bp) {  //
    int i, y, x;
    piece *ipip, *pip;
    ipip = initPieces;
    pip = bp->pieces;
    player *pp = bp->players;
    pp->direction = -1;
    pp++;
    pp->direction = 1;

    for (y = 0; y < MAX_Y; y++) {
        for (x = 0; x < MAX_X; x++) {
            bp->squares[y][x] = EMP;
        }
    }

    for (i = 0; i < MAX_PIECES; i++) {
        // 盤を設定する処理
        updatePieceString(bp, ipip);  // コマ文字列をアップデート
        bp->squares[ipip->y][ipip->x] = i;
        *pip++ = *ipip++;  // 駒のデータを一括コピー
    }
}

void display(board *bp) {
    for (int y = 0; y < MAX_Y; y++) {
        for (int x = 0; x < MAX_X; x++) {
            int p = bp->squares[y][x];
            printf("%s ", p == -1 ? "...." : bp->pieces[p].pieceString);
        }
        printf("\n");
    }
}